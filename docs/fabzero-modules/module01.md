# Gestion de projet et documentation
## 1. Objectives
This week I worked on defining my final project idea and started to getting used to the documentation process.
- [x]  "OBJECTIF DE LA SEMAINE RECAP"

## Git stuff
1. Configurer git:
2. Pusher les informations:
```
JUST CODING
```
git pull #download the latest change on the project

git add -A #add all changes to commit
git commit -m #comment the patchnote

git push #push to the server the patchnote


## Visual studio code

1. Pourquoi cette hardware ?  
Ah, la fameuse question de l'editeur de texte ! Dans le cadre de mes projets fablabs j'utiliserait principalement **Visual Studio Code**. Visual studio code est un logiciel de traitement de texte relativement similaire à d'autres logiciel de traitement de texte (_notepad ++_, _world_, ...) à la différence que celui ci compile en un logiciel un terminal et un module de prévisualisation (très utile lorsqu'on travaille en markdown).  
Le choix de ce logiciel est donc un pur caprice ergonomique.  
Des logiciels comme "_Notepad ++_" couplés à un terminal externe (comme _Git Bash_) sont donc tout à faits adaptés aux différentes tâches demandés mais souffre d'une ergonomie plus archaïque limitant le confort dans un usage quotidien.

1. Maitriser la bête :



## Markdown



## Table 1

| Qty |  Description    |  Price  |           Link           | Notes  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Material one    |  22.00 $| http://amazon.com/test   |    Order many    |
| 1   | Material two    |  22.00 $| http://amazon.com/test   |        |
| 1   | Material three  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material five   |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eight  |  22.00 $| http://amazon.com/test   |        |
| 1   | Material twelve |  22.00 $| http://amazon.com/test   |        |
| 1   | Material eleven |  22.00 $| http://amazon.com/test   |        |

## Useful links

- [Docsify](https://docsify.js.org/#/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)


## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```


## Image gallery

![](../images/fablab-machine-logos.svg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3oItpVa9fs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 3D Models

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/658c8f8a2f3042c3ad7bdedd83f1c915?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Dita&#39;s Gown</a>
    by <a href="https://sketchfab.com/francisbitontistudio?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Francis Bitonti Studio</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>
